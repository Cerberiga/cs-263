import sys, pygame
import time
import math
import random
import copy
import pybrain
import Prey
import Animat
import Screen
import Barrier
import Fruit
import os
import calendar

pygame.init()

rotating = 0
move = 0

other_move = 0
other_rotate = 0
movt = 0

# Get the squared rectilinear distance between two coordinates.
def rectDistance(c1, c2):
  return math.pow(c2[1] - c1[1],2) + math.pow(c2[0] - c1[0],2)

# Convert an angle to a unit vector in the same direction.
def getXY(angle, speed_val):
  if angle%360 == 90:
    return [0, -speed_val]
  elif angle%360 == 270:
    return [0, speed_val]
  else:
    inv = 1
    if angle%360 > 89 and angle%360 <= 269:
      inv = -1
    rad = angle * ((2*math.pi)/360)
    tan_t = math.tan(rad)
    t2 = math.pow(tan_t,2)
    x = math.sqrt(1 / (t2 + 1))
    y = -( x * tan_t)
    return [inv * x * speed_val, inv * y * speed_val]

# Wrap around angles that are greater than 360.
def prep_angle_round(angle):
  int_angle = int(angle)
  return int_angle%360

if __name__ == "__main__":
  screen = Screen.Screen(800, 600)
  screen.prep_predator()
  screen.prep_prey()
  screen.prep_barriers()
  screen.prep_barriers_list()
  screen.prep_fruit()
  screen.prep_fruit_list()
  screen.load_prey()
  screen.load_predators()
  count = 0
  time_diff = 0
  time_sum = 0
  while 1:
    if (len(screen.m_predators) == 0 and len(screen.m_prey) == 0):
      break
    count+=1;
    os.system('clear')
    time_diff = time.time()

    screen.update()
    screen.m_screen.fill(screen.m_background_color)
    screen.display_fruit()
    screen.display_predators()
    screen.display_prey()
    screen.display_barriers()
    
    pygame.display.flip()
    time_sum += time.time() - time_diff
    print time_sum/count
