import sys, pygame 
import time 
import math 
import random 
import copy 
import pybrain 
import Animat 
import Screen 
import Barrier
import Main

from pybrain.structure import FeedForwardNetwork, FullConnection, LinearLayer, SigmoidLayer, SoftmaxLayer
from pybrain.datasets import ImportanceDataSet
from pybrain.supervised.trainers import BackpropTrainer

WANDER = 0
IDLE = 1
MOVE_TO = 2
EAT_FRUIT = 3
RETREAT = 4
FLEE = 5
EAT_ANIMAT = 6
MOVE_TO_PRED = 7
MOVE_TO_PREY = 8

FRUIT_ENERGY = 200
PREY_ENERGY = 1000

#The default Animat class that contains Predator information. The Prey inherit from this class but overrides the learning.
class Animat:
  pos = [0, 0]
  m_left_x = 0
  m_top_y = 0
  m_width = 0
  m_height = 0
  max_speed = 10
  curr_speed = 10
  curr_movt = [0, 0]
  curr_direct = [0, -10]
  angle_deg = 90
  moving = 0
  rotating = 0
  rotate_clock = 0
  rotate_rest = 0
  move_clock = 0
  move_rest = 0
  sensory_range = 150 
  default_image = None
  m_rect = None
  current_image = None
  multiplier = 500 # key!
  energy = multiplier*3
  health = multiplier*3
  hungryRatio = 0.6

  m_animat_type = 0

  seesFruit = None
  m_seesFruitDist = -1

  m_sees_pred = None
  m_sees_pred_dist = -1
  m_touch_pred = None  
  m_chasing_pred = False 

  m_sees_prey = None
  m_sees_prey_dist = -1
  m_touch_prey = None  
  m_chasing_prey = False

  m_chased_by = set()

  touchesFruit = None

  state = IDLE
  state_info = {};
  screen = None;
  timeInState = 1
  outputs = ()
  
  timeAlive = 1
  numOfFruitsEaten = 0
  
  def start_being_chased(self, predator):
    self.m_chased_by.add(predator)
  
  def stop_being_chased(self, predator):
    if predator in self.m_chased_by:
      self.m_chased_by.remove(predator)
      return 1
    else:
      return 0

  def __init__(self):
    self.m_width = 15
    self.m_height = 15
   
    # Neural Network
    network = FeedForwardNetwork()
    self.inputNodes = 10
    self.outputNodes = 9
    self.hiddenNodes = int(self.inputNodes * 1.5)
    inLayer = LinearLayer(self.inputNodes, name='in')
    hiddenLayer = SigmoidLayer(self.hiddenNodes, name='hidden')
    outLayer = SoftmaxLayer(self.outputNodes, name='output')
    network.addInputModule(inLayer)
    network.addModule(hiddenLayer)
    network.addOutputModule(outLayer)
    network.addConnection(FullConnection(inLayer, hiddenLayer))
    network.addConnection(FullConnection(hiddenLayer, outLayer))
    network.sortModules()
    self.network = network
    
  # Whenever Animat.update is called, inputs are fed to the neural network which produces the output state that the Animat should be in. Additionally, back propagation is performed depending on whether the Animat's decision was appropriate.
  def update(self):
    seesFruit = self.seesFruit
    touchesFruit = self.touchesFruit

    seesPred = self.m_sees_pred
    touchesPred = self.m_touch_pred
 
    seesPrey = self.m_sees_prey
    touchesPrey = self.m_touch_prey

    isPreyBeingChased = self.is_prey_being_chased()

    inputs = (seesPred != None,
              touchesPred != None,
              seesPrey != None,
              touchesPrey != None,
              isPreyBeingChased,
              self.energy < self.multiplier * self.hungryRatio,
              seesFruit != None,
              touchesFruit != None,
              self.multiplier * self.timeInState,
              self.multiplier * self.state)
    outputs = self.network.activate(inputs)
    self.outputs = outputs
    if (self.health > 0):
      ds = ImportanceDataSet(self.inputNodes, self.outputNodes)
      state = outputs.argmax()

      if touchesPrey != None:
        if (state != EAT_ANIMAT):
          ds.addSample(inputs, (0,0,0,0,0,0,1,0,0), 100)
        else:
          ds.addSample(inputs, outputs)
      elif touchesPrey == None and state == EAT_ANIMAT:
        if (isPreyBeingChased):
          ds.addSample(inputs, (0,0,0,0,0,0,0,0,1), 100)
        else:
          ds.addSample(inputs, (1,1,0,0,1,0,0,0,0), 5)
      elif (self.touchesFruit != None):
        if (state != EAT_FRUIT):
          ds.addSample(inputs, (0,0,0,1,0,0,0,0,0), 100)
        else:
          ds.addSample(inputs, outputs)
      elif (self.touchesFruit == None and state == EAT_FRUIT):
        ds.addSample(inputs, (1,1,0,0,1,0,0,0,0), 5)
      elif (self.seesFruit != None):
        if (state != MOVE_TO):
          ds.addSample(inputs, (0,0,1,0,0,0,0,0,0), 10)
        elif (state == MOVE_TO and self.timeInState > 200):
          ds.addSample(inputs, (1,0,0,0,0,0,0,0,0), 1000)
        elif (state == MOVE_TO):
          ds.addSample(inputs, outputs)
      elif (self.seesFruit == None and state == MOVE_TO):
        ds.addSample(inputs, (1,1,0,0,1,0,0,0,0), 0.5)
      elif seesPred != None and seesPrey:
        if (state != MOVE_TO_PREY):
          ds.addSample(inputs, (0,0,0,0,0,0,0,0,1), 100)
        else:
          ds.addSample(inputs, outputs)
      elif seesPred != None and not seesPrey:
        ds.addSample(inputs, (.5,0,0,0,0,0,0,.5,0), 100)
      else:
        ds.addSample(inputs, (.5,0,0,0,0,0,0,0,0), 1)
      self.set_state(state)
      
      trainer = BackpropTrainer(self.network, ds)
      trainer.train()
    else:
      self.set_idle()
    
    
  # Rotate the animat by the degree amount specified by amt.
  # amt > 0, it rotates counter clockwise (left)
  # amt < 0, it rotates clockwise (right)
  def rotate(self, amt):
    self.angle_deg = Main.prep_angle_round(self.angle_deg + amt)
    self.curr_direct = Main.getXY(self.angle_deg, self.curr_speed)
    self.current_image = pygame.transform.rotate(self.default_image, self.angle_deg - 90)

  # Turn the animat to face an absolute direction.
  def abs_rotate(self, angle):
    self.angle_deg = angle
    self.curr_direct = Main.getXY(angle, self.curr_speed)
    self.current_image = pygame.transform.rotate(self.default_image, angle - 90)
    
  # Move forward or backward based on the speed of the Animat. fb is 1 for forward, -1 for backwards.
  def move(self, fb, screen):
    curr_pos = self.pos
    temp = [elem*fb for elem in self.curr_direct]
    x = curr_pos[0] + int(temp[0])
    y = curr_pos[1] + int(temp[1])
    temp_rect = self.m_rect.move(temp)
    overlapping = screen.check_overlap(temp_rect)
    if x > 0 and x + self.m_width < screen.m_width and y > 0 and y + self.m_height < screen.m_height and overlapping == -1: 
      self.pos = [x, y]
      self.m_left_x = x
      self.m_top_y = y
      self.m_rect = temp_rect
      return 1
    else:
      temp_rect_y = self.m_rect.move([0, temp[1]])
      temp_rect_x = self.m_rect.move([temp[0], 0])
      if abs(temp[0]) > 1 and x > 0 and x + self.m_width < screen.m_width and curr_pos[1] > 0 and curr_pos[1] + self.m_height < screen.m_height and screen.check_overlap(temp_rect_x) == -1:
        self.pos = [x, curr_pos[1]]
        self.m_left_x = x
        self.m_top_y = curr_pos[1]
        self.m_rect = temp_rect_x
        return 0
      elif abs(temp[1]) > 1 and curr_pos[0] > 0 and curr_pos[0] + self.m_width < screen.m_width and y > 0 and y + self.m_height < screen.m_height and screen.check_overlap(temp_rect_y) == -1:
        self.pos = [curr_pos[0], y]
        self.m_left_x = curr_pos[0]
        self.m_top_y = y
        self.m_rect = temp_rect_y
        return 0
    return 0

  # Move to the coordinates specified by tx and ty. This is accomplished via rotating, moving, or rotating and moving depending on which set of actions will take the Animat closer to the goal.
  def follow(self, tx, ty, tw, th, rect, screen):
    target_x = int(tx + tw/2)
    target_y = int(ty + th/2)
    diff_x = target_x - int(self.pos[0] + self.m_width/2)
    diff_y = -(target_y - int(self.pos[1] + self.m_height/2))
    final_angle_diff = self.turn_to_face(diff_x, diff_y)
    prev_pos = copy.copy(self.pos)    
    dist = Main.rectDistance(prev_pos, [tx, ty])
    if (final_angle_diff < 8 or math.sqrt(dist) > (self.curr_speed * 180)/(8*math.pi)):
      self.move(1, screen)

  # Turn the animat to face the direction specified by diff_x and diff_y which is the difference between the animat's current position and the target that it should turn towards. 
  def turn_to_face(self, diff_x, diff_y):
    if diff_x == 0:
      if diff_y > 0:
        target_angle = math.pi/2
      else:
        target_angle = -math.pi/2
    else:
      target_angle = math.atan(float(diff_y) / float(diff_x))
    deg_angle = (target_angle * 180) / math.pi

    if diff_x < 0:
      if diff_y > 0:
        deg_angle = 180 + deg_angle
      else:
        deg_angle = -180 + deg_angle
    temp_target_angle = deg_angle

    offset = 0
    if self.angle_deg%360 > 180:
      offset = 360
    temp_self_angle = self.angle_deg%360 - offset
    final_angle_diff = 0
    if (temp_target_angle > 0) == (temp_self_angle > 0):
      angle_diff = temp_target_angle - temp_self_angle
      final_angle_diff = abs(angle_diff)
      if abs(angle_diff) < 8:
        self.abs_rotate(Main.prep_angle_round(deg_angle))
      elif angle_diff > 0:
        self.rotate(8)
      else:
        self.rotate(-8)
    else:
      inv = 1
      if temp_target_angle < 0:
        inv = -1
      cw = inv *(temp_target_angle - temp_self_angle)
      ccw = 360 + inv*(temp_self_angle - temp_target_angle)
      final_angle_diff = min(abs(cw), abs(ccw))
      if min(abs(cw), abs(ccw)) < 8:
        self.abs_rotate(Main.prep_angle_round(deg_angle))
      elif cw < ccw:
        self.rotate(inv*8)
      else:
        self.rotate(inv*-8)
    return final_angle_diff 

  # Flee (move away from) from the set of animats in the pursuers set. When close to the wall, consider the wall as a target to flee from so that the animat does not get cornered too easily.
  def flee_from(self, pursuers):
    diff_x = 0
    diff_y = 0
    if self.m_left_x < 30: 
      diff_x += 100  
    elif self.screen.m_width - self.m_left_x < 30:
      diff_x -= 100

    if self.m_top_y < 30:
      diff_y -= 100
    elif self.screen.m_height - self.m_top_y < 30:
      diff_y += 100

    self_x = int(self.m_left_x + self.m_width/2)
    self_y = int(self.m_top_y + self.m_height/2)
  
    temp_x = 0
    temp_y = 0
    for elem in pursuers:
      target_x = int(elem.m_left_x + elem.m_width/2)
      target_y = int(elem.m_top_y + elem.m_height/2)
      temp_diff_x = -(target_x - self_x)
      temp_diff_y = target_y - self_y
      inv_x = (int(temp_diff_x >= 0)<<1) - 1
      inv_y = (int(temp_diff_y >= 0)<<1) - 1
      temp_x += inv_x * self.screen.m_width - temp_diff_x
      temp_y += inv_y * self.screen.m_height - temp_diff_y
    inv_x = (int(temp_x >= 0)<<1) - 1
    inv_y = (int(temp_y >= 0)<<1) - 1
    diff_x += inv_x * self.screen.m_width - temp_x
    diff_y += inv_y * self.screen.m_height - temp_y
    self.flee_from_angle(diff_x, diff_y)

  # Given an difference between the animat and a target, move away from that target.
  def flee_from_angle(self, diff_x, diff_y):
    self.turn_to_face(diff_x, diff_y)
    self.move(1, self.screen)

  # Does the animat see a fruit? If so, return the distance between the animat and the fruit.
  def f_see(self, fruit):
    dist = math.sqrt(Main.rectDistance([fruit.m_middle_x, fruit.m_middle_y], [self.m_left_x, self.m_top_y]))
    if dist < self.sensory_range:
      return dist
    else:
      return -1

  # Does the animat touch a fruit?
  def f_touch(self, fruit):
    if self.m_rect.colliderect(fruit.m_rect):
      return 1
    else:
      return 0

  # Perform the wander action.
  def do_wander(self):
    self.changeEnergy(-2)
    if self.rotate_clock == 0:
      self.move_clock = self.move_clock ^ 1
      if self.move_clock:
        self.rotate_clock = random.randint(0, 60) 
        if self.move(1, self.screen) == 0:
          self.rotate_clock = 0
      else:
        self.rotate_clock = random.randint(0, 20)
        self.rotating = 10*(random.randint(0, 1)*2 - 1)
        self.rotate(self.rotating)    
    else:
      if self.move_clock:
        if self.move(1, self.screen) == 0:
          self.rotate_clock = 1
      else:
        self.rotate(self.rotating)
      self.rotate_clock -= 1 
        
  # Do nothing.
  def do_idle(self):
    self.changeEnergy(-1)
    return

  # Move the animat to the fruit that it sees.
  def do_move_to_fruit(self):
    self.changeEnergy(-2)
    if self.seesFruit != None:
      self.follow(self.seesFruit.m_left_x, self.seesFruit.m_top_y, self.seesFruit.m_width, self.seesFruit.m_height, self.seesFruit.m_rect, self.screen)
  
  # Move the animat to the predator that it sees.
  def do_move_to_pred(self):
    self.changeEnergy(-2)
    if self.m_sees_pred != None:
      self.follow(self.m_sees_pred.m_left_x, self.m_sees_pred.m_top_y, self.m_sees_pred.m_width, self.m_sees_pred.m_height, self.m_sees_pred.m_rect, self.screen)  

  # Move the animat to the prey that it sees.
  def do_move_to_prey(self):
    self.changeEnergy(-2)
    if self.m_sees_prey != None:
      self.follow(self.m_sees_prey.m_left_x, self.m_sees_prey.m_top_y, self.m_sees_prey.m_width, self.m_sees_prey.m_height, self.m_sees_prey.m_rect, self.screen)  
      self.m_chasing_prey = True
      self.m_sees_prey.start_being_chased(self)

  # Have the animat flee from the predator that it sees.
  def do_flee(self):
    real_set = set()
    self.changeEnergy(-0.5)
    if self.m_sees_pred != None:
      real_set.add(self.m_sees_pred)
    if len(real_set) > 0:
      self.flee_from(real_set)

  # Is the prey that this animat sees currently being chased by another animat?
  def is_prey_being_chased(self):
    if self.m_sees_prey != None:
      return len(self.m_sees_prey.m_chased_by) & 1
    else:
      return 0

  # Eat the fruit that this animat is touching.
  def do_eat_fruit(self):
    if self.touchesFruit != None:
      if self.screen.remove_fruit(self.touchesFruit):
        self.numOfFruitsEaten += 1
        self.screen.m_total_num_of_fruits_eaten += 1
        self.changeEnergy(FRUIT_ENERGY)
      else:
        self.changeEnergy(-1)
      self.touchesFruit = None
      
  # Eat the animat that this animat is touching.
  def do_eat_animat(self):
    if self.m_touch_prey != None:
      chased_set = self.m_touch_prey.m_chased_by
      if self.m_touch_prey.m_animat_type == 1:
        if self.screen.remove_prey(self.m_touch_prey):
          for elem in chased_set:
            elem.changeEnergy(PREY_ENERGY)
        else:
          self.changeEnergy(-1)    
        self.state_info = None
        self.m_touch_prey = None

  # Back up.
  def do_retreat(self):
    self.move(-1, self.screen)
    
  def set_state(self, state):
    if self.state == MOVE_TO_PREY and state != MOVE_TO_PREY:
      if self.m_sees_prey != None:
        self.m_sees_prey.stop_being_chased(self)
    self.updateTimeInState(state)
    self.state = state

  def set_idle(self):
    self.updateTimeInState(IDLE)
    self.state = IDLE

  def updateTimeInState(self, state):
    if (self.state != state):
      self.timeInState = 0
    self.timeInState += 1

  # Change the energy of the Animat by the amount specified by "value". If its energy falls below a certain threshold, its health will suffer as a result.    
  def changeEnergy(self, value=0):
    self.energy += value
    if (self.energy > self.multiplier*3):
      self.energy = self.multiplier*3
    if (self.energy < self.multiplier*3 * self.hungryRatio):
     
      # decrement health (perhaps exponentially later on)
      if (self.health > 0 and self.energy < 10):
        # handle case where it is running low on energy, so it trades some health for energy
        self.changeHealth(-5)
        if (self.health > 0):
          self.changeEnergy(10)
      else:
        self.changeHealth(-1)
    else:
      # slowly recover health (perhaps exponentially later on)
      self.changeHealth(1)
      
  def changeHealth(self, value=0):
    self.health += value
    if (self.health > self.multiplier*3):
      self.health = self.multiplier*3

  def perform_action(self):
    self.timeAlive += 1
    if self.state == WANDER:
      self.do_wander()
    elif self.state == IDLE:
      self.do_idle()
    elif self.state == MOVE_TO:
      self.do_move_to_fruit()
    elif self.state == EAT_FRUIT:
      self.do_eat_fruit()
    elif self.state == RETREAT:
      self.do_retreat()
    elif self.state == FLEE:
      self.do_flee()
    elif self.state == EAT_ANIMAT:
      self.do_eat_animat()
    elif self.state == MOVE_TO_PRED:
      self.do_move_to_pred()
    elif self.state == MOVE_TO_PREY:
      self.do_move_to_prey()
      
  def get_state_string(self):
    if self.state == 0:
      return "WANDER"
    elif self.state == 1:
      return "IDLE"
    elif self.state == 2:
      return "MOVE_TO"
    elif self.state == 3:
      return "EAT_FRUIT"
    elif self.state == 4:
      return "RETREAT" 
    elif self.state == 5:
      return "FLEEING"
    elif self.state == EAT_ANIMAT:
      return "EAT_ANIMAT"
    elif self.state == MOVE_TO_PRED:
      return "MOVE TO PRED"
    elif self.state == MOVE_TO_PREY:
      return "MOVE TO PREY"
