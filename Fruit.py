import sys, pygame 
import time 
import math 
import random 
import copy 
import pybrain 
import Animat 
import Screen 
import Barrier

class Fruit:
  m_left_x = 0
  m_top_y = 0
  m_width = 0
  m_height = 0
  m_rect = None
  m_surface = None
  m_middle_x = 0
  m_middle_y = 0
  
  def __init__(self, x, y, width, height):
    self.m_left_x = x
    self.m_top_y = y
    self.m_width = width
    self.m_height = height
    self.m_middle_x = x + width/2;
    self.m_middle_y = y + height/2;
    self.m_surface = pygame.Surface((width, height))
    self.m_surface.fill((0,255,0))
    self.m_rect = pygame.Rect(x, y, width, height)
