import sys, pygame
import math
import random
import pybrain
import Main
import Animat

from pybrain.structure import FeedForwardNetwork, FullConnection, LinearLayer, SigmoidLayer, SoftmaxLayer
from pybrain.datasets import ImportanceDataSet
from pybrain.supervised.trainers import BackpropTrainer

WANDER = 0
IDLE = 1
MOVE_TO = 2
EAT_FRUIT = 3
RETREAT = 4
FLEE = 5

FRUIT_ENERGY = 200

class Prey(Animat.Animat):
  m_animat_type = 1
  sensory_range = 150
  curr_speed = 8
  max_speed = 20
  incr_speed = 1
  multiplier = 200 # key!
  energy = multiplier
  health = multiplier
  
  def __init__(self, screen, coords=None):
    self.m_width = 15
    self.m_height = 15
    self.screen = screen
    
    self.default_image = screen.m_prey_image
    x = 0
    y = 0
    if coords != None:
      x = coords[0] 
      y = coords[1] 
    else:
      x = random.randint(1,screen.m_width - self.m_width - 1)
      y = random.randint(1,screen.m_height - self.m_height- 1)
    angle = random.randint(0, 44)*8
    self.pos = [x, y]
    self.m_left_x = x
    self.m_top_y = y
    self.angle_deg = angle
    self.curr_direct = Main.getXY(angle, self.curr_speed)
    self.m_rect = pygame.Rect(x,y,15,15)
    self.current_image = pygame.transform.rotate(self.default_image, self.angle_deg - 90)

    network = FeedForwardNetwork()
    self.inputNodes = 6
    self.outputNodes = 6
    self.hiddenNodes = int(self.inputNodes * 1.5)
    inLayer = LinearLayer(self.inputNodes, name='in')
    hiddenLayer = SigmoidLayer(self.hiddenNodes, name='hidden')
    outLayer = SoftmaxLayer(self.outputNodes, name='output')
    network.addInputModule(inLayer)
    network.addModule(hiddenLayer)
    network.addOutputModule(outLayer)
    network.addConnection(FullConnection(inLayer, hiddenLayer))
    network.addConnection(FullConnection(hiddenLayer, outLayer))
    network.sortModules()
    self.network = network

  def update(self):
    seesFruit = self.seesFruit
    touchesFruit = self.touchesFruit
    beingChased = self.m_sees_pred != None
    inputs = (self.energy < self.multiplier * self.hungryRatio,
              seesFruit != None,
              touchesFruit != None,
              self.multiplier * self.timeInState,
              self.multiplier * self.state,
              beingChased)
    outputs = self.network.activate(inputs)
    self.outputs = outputs
    if (self.health > 0):
      ds = ImportanceDataSet(self.inputNodes, self.outputNodes)
      state = outputs.argmax()
      if beingChased and state != FLEE:
        ds.addSample(inputs, (0,0,0,0,0,1), 100)
      elif (self.touchesFruit != None):
        if (state != EAT_FRUIT):
          ds.addSample(inputs, (0,0,0,1,0,0), 100)
        else:
          ds.addSample(inputs, outputs)
        self.set_state(state)
      elif (self.touchesFruit == None and state == Animat.EAT_FRUIT):
        ds.addSample(inputs, (1,1,0,0,1,0), 5)
        self.set_state(Animat.IDLE)
      elif (self.seesFruit != None):
        if (state != Animat.MOVE_TO):
          ds.addSample(inputs, (0,0,1,0,0,0), 10)
        elif (state == Animat.MOVE_TO and self.timeInState > 200):
          ds.addSample(inputs, (1,0,0,0,0,0), 1000)
        elif (state == Animat.MOVE_TO):
          ds.addSample(inputs, outputs)
        self.set_state(state)
      elif (self.seesFruit == None and state == Animat.MOVE_TO):
        ds.addSample(inputs, (1,1,0,0,1,0), 0.5)
        self.set_state(Animat.IDLE)
      elif (self.touchesFruit == None and state == EAT_FRUIT):
        ds.addSample(inputs, (1,1,0,0,1,0), 5)
        self.set_state(IDLE)
      elif (self.seesFruit != None):
        if (state != MOVE_TO):
          ds.addSample(inputs, (0,0,1,0,0,0), 10)
        elif (state == MOVE_TO and self.timeInState > 200):
          ds.addSample(inputs, (1,0,0,0,0,0), 1000)
        elif (state == MOVE_TO):
          ds.addSample(inputs, outputs)
        self.set_state(state, self.seesFruit)
      elif (self.seesFruit == None and state == MOVE_TO):
        ds.addSample(inputs, (1,1,0,0,1,0), 0.5)
        self.set_state(IDLE)
      else:
        ds.addSample(inputs, (1,0,0,0,0,0), 1)
        self.set_state(state)
      trainer = BackpropTrainer(self.network, ds)
      trainer.train()
    else:
      self.set_idle()
      
  def perform_action(self):
    self.timeAlive += 1
    if self.state == WANDER:
      self.do_wander()
    elif self.state == IDLE:
      self.do_idle()
    elif self.state == MOVE_TO:
      self.do_move_to_fruit()
    elif self.state == EAT_FRUIT:
      self.do_eat_fruit()
    elif self.state == RETREAT:
      self.do_retreat()
    elif self.state == FLEE:
      self.do_flee()
      
  def get_state_string(self):
    if self.state == 0:
      return "WANDER"
    elif self.state == 1:
      return "IDLE"
    elif self.state == 2:
      return "MOVE_TO"
    elif self.state == 3:
      return "EAT_FRUIT"
    elif self.state == 4:
      return "RETREAT" 
    elif self.state == 5:
      return "FLEEING"
   
  # Given two possible speeds a and b, choose the fastest of the two and increment based on fixed generational speed increase.   
  def update_speed(self, a = 0, b = 0):
    self.curr_speed = max(a, b) + self.incr_speed
    if (self.curr_speed > self.max_speed):
      self.curr_speed = self.max_speed
    self.rotate(0)
