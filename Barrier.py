import sys, pygame 
import time 
import math 
import random 
import copy 
import pybrain 
import Animat 
import Screen 
import Barrier

class Barrier:
  m_left_x = 0
  m_top_y = 0
  m_width = 0
  m_height = 0
  m_rect = None
  m_surface = None
  def __init__(self, x, y, width, height):
    self.m_left_x = x
    self.m_left_y = y
    self.m_width = width
    self.m_height = height
    self.m_surface = pygame.Surface((width, height))
    self.m_surface.fill((255,255,255))
    self.m_rect = pygame.Rect(x, y, width, height)
