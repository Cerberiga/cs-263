# CS 263C

The goal of this project is have the predator animats learn to cooperate in hunting the prey animats in a 2D environment.

The predators and prey use a neural network to learn from their interaction with the environment.

## Requirements/Installation

* Requires Python 2.7.6
* Requires Pygame - "sudo apt-get install python-pygame"
* Requires Pybrain - installed using the following guide "http://pybrain.org/docs/quickstart/installation.html"

## Usage

From the command line in this directory:

> python Main.py

The Screen class defines the initial number of prey, predator, and fruit for those that want to change the starting factors.

The red pointer-looking object represents the predator animat. These animats will not replenish over time.

The blue pointer-looking object represents the prey animats. The animats will always repopulate.

The green square object represents the fruit. The refresh rate of fruits will go down over time.

The white rectangular object represents the barrier.

## Classes

The Main class contains the start logic, such as creating the screen object. The main repeatedly tells the screen to update and refresh its state to reflect changes in the drawing.

The Screen class contains all the functions that specific animats do not have to do and maintains the overall list of predator, prey, fruit, and barrier objects in a list. For example, it handles the repopulation of fruits, checking for collision, checking the sense field, etc. Most importantly, the screen tells each of the animats to update their state.

The Animat class is the predator animat. The predator animat utilizes back propogation to update its neural network.

The Prey class is a subclass of Animat and represents the prey animat. The prey animat uses generational learning to update its neural network.