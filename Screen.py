import sys, pygame 
import time 
import math 
import random 
import copy 
import pybrain 
import Animat 
import Screen 
import Barrier
import Main
import Fruit
import Prey

#STATES: WANDER, IDLE, MOVE_TO, EAT_FRUIT, RETREAT
#F_SEE, P_SEE
# FRUITS RANDOM
# HAVE T/F for one fruit vs. many
# HAVE VARIABLE INDICATING HOW LONG ITS BEEN IN A PARTICULAR STATE

PREDATOR = 0
PREY = 1

class Screen:
  m_width = 0
  m_height = 0
  m_screen = None
  m_background_color = 50, 50, 50
  m_predator_image = None
  m_blue = None
  m_green = None
  m_predators = []
  m_max_predators = 4

  m_prey = []
  m_prey_image = None
  m_stable_prey_number = 0
  m_max_prey = 20

  m_barriers = []
  m_barriers_list = []
  m_fruit = []
  m_fruit_list = []
  m_max_fruit = 30
  m_total_num_of_fruits_eaten = 0

  def __init__(self, width, height):
    self.m_width = width
    self.m_height = height
    self.m_screen = pygame.display.set_mode((width, height))

  # Initialize functions for the objects and barriers. 
  def prep_barriers(self):
    self.m_barriers.append(Barrier.Barrier(300, 200, 20, 200))
    self.m_barriers.append(Barrier.Barrier(520, 200, 20, 200))
  def prep_barriers_list(self):
    for elem in self.m_barriers:
      self.m_barriers_list.append(elem.m_rect)
  def prep_fruit(self):
    for i in range(0, self.m_max_fruit):
      coords = self.get_safe_coordinates()
      new_fruit = Fruit.Fruit(coords[0], coords[1], 15, 15)
      self.m_fruit.append(new_fruit)
  def prep_fruit_list(self):
    for elem in self.m_fruit:
      self.m_fruit_list.append(elem.m_rect)
  def prep_prey(self):
    self.m_prey_image = pygame.image.load("animat3.png").convert()
    self.m_prey_image.set_colorkey((255,255,255))
  def load_prey(self):
    num_prey = self.m_max_prey
    self.m_stable_prey_number = num_prey
    for i in range(0, num_prey):
      temp = Prey.Prey(self, self.get_safe_coordinates())
      self.m_prey.append(temp)
  def prep_predator(self):
    self.m_predator_image = pygame.image.load("animat2.png").convert()
    self.m_predator_image.set_colorkey((255,255,255))
  def load_predators(self):
    num_pred = self.m_max_predators
    for i in range(0, num_pred):
      temp = Animat.Animat()
      temp.default_image = self.m_predator_image
      coords = self.get_safe_coordinates()
      x = coords[0]   
      y = coords[1]
      angle = random.randint(0, 44)*8
      temp.screen = self
      temp.pos = [x, y]
      temp.m_left_x = x
      temp.m_top_y = y
      temp.angle_deg = angle
      temp.curr_direct = Main.getXY(angle, temp.curr_speed)
      temp.m_rect = pygame.Rect(x,y,15,15)
      temp.current_image = pygame.transform.rotate(temp.default_image, temp.angle_deg - 90)
      self.m_predators.append(temp)

  def display_predators(self):
    for i in range(0, len(self.m_predators)):
      self.m_screen.blit(self.m_predators[i].current_image, self.m_predators[i].m_rect)

  def display_prey(self):
    for i in range(0, len(self.m_prey)):
      self.m_screen.blit(self.m_prey[i].current_image, self.m_prey[i].m_rect)

  def display_barriers(self):
    for i in range(0, len(self.m_barriers)):
      self.m_screen.blit(self.m_barriers[i].m_surface, self.m_barriers[i].m_rect)
  def display_fruit(self):
    for i in range(0, len(self.m_fruit)):
      self.m_screen.blit(self.m_fruit[i].m_surface, self.m_fruit[i].m_rect)

  # Check if the coordinates specified by rect overlaps with any of the barriers.
  def check_overlap(self, rect):
    return rect.collidelist(self.m_barriers_list)

  # Check each animat's sense field and update what they see/sense.
  def check_sense_fields2(self):
    unified = self.m_predators + self.m_prey
    for i in range(0, len(unified)):
      generic = unified[i]
      generic.seesFruit = None
      generic.touchesFruit = None
      generic.m_seesFruitDist = 10000

      generic.m_sees_pred = None
      generic.m_sees_pred_dist = 10000
      generic.m_touch_pred = None

      generic.m_sees_prey = None
      generic.m_sees_prey_dist = 10000
      generic.m_touch_prey = None

    
    for i in range(0, len(unified)):
      generic = unified[i]
      for j in range(i+1, len(unified)):
        self.bidirectional_update(generic, unified[j])

      for j in range(0, len(self.m_fruit)):
        if generic.f_touch(self.m_fruit[j]):
          generic.touchesFruit = self.m_fruit[j]
        else:
          dist = generic.f_see(self.m_fruit[j])
          if dist != -1 and dist < generic.m_seesFruitDist: 
            generic.seesFruit = self.m_fruit[j]
            generic.m_seesFruitDist = dist
      
  # Given two animats anim1 and anim2, determine whether they can see or are touching each other and update their variables.
  def bidirectional_update(self, anim1, anim2):
    if anim1.f_touch(anim2):
      self.set_touch(anim1, anim2)
    else:
      dist = math.sqrt(Main.rectDistance([anim1.m_left_x, anim1.m_top_y], [anim2.m_left_x, anim2.m_top_y]))
      self.set_sight(anim1, anim2, dist)
      self.set_sight(anim2, anim1, dist)

  # If anim1 sees anim2, update anim1 with this knowledge.
  def set_sight(self, anim1, anim2, dist):
    if dist < anim1.sensory_range:
      if anim2.m_animat_type == PREDATOR and dist < anim1.m_sees_pred_dist:
        anim1.m_sees_pred = anim2
        anim1.m_sees_pred_dist = dist

      elif dist < anim1.m_sees_prey_dist:
        anim1.m_sees_prey = anim2
        anim1.m_sees_prey_dist = dist

  # If anim1 and anim2 are touching, update both animats with this knowledge.
  def set_touch(self, anim1, anim2):
    if anim2.m_animat_type == PREDATOR:
      anim1.m_touch_pred = anim2
    else:
      anim1.m_touch_prey = anim2
    if anim1.m_animat_type == PREDATOR:
      anim2.m_touch_pred = anim1
    else:
      anim2.m_touch_prey = anim1

  # Called at each time unit. Update the animats' sensory fields and also have each perform the appropriate action at this time step.
  def update(self):
    self.refresh_fruit()
    self.check_sense_fields2()
    length = len(self.m_predators)
    for i in reversed(range(0, length)):
      predator = self.m_predators[i]
      predator.update()
      predator.perform_action()
      if (predator.health <= 0):
        self.remove_predator(predator)

    for i in reversed(range(0, len(self.m_prey))):
      prey = self.m_prey[i]
      prey.update()
      prey.perform_action()
      if (prey.health <= 0):
        self.remove_prey(prey)
        
    while (len(self.m_prey) < self.m_max_prey and len(self.m_prey) > 0):
      rand = random.randint(0,100)
      # TODO: find a suitable number to probabilistically spawn prey
      if (rand < 0):
        break
      temp = Prey.Prey(self, self.get_safe_coordinates())
      parent1_speed = temp.curr_speed
      parent2_speed = temp.curr_speed
      if (len(self.m_prey) != 1):
        # TODO: change lambda to not just include numOfFruitsEaten
        bestPreys = sorted(self.m_prey, key=lambda prey: -prey.numOfFruitsEaten)
        parent1_speed = bestPreys[0].curr_speed
        parent2_speed = bestPreys[1].curr_speed
        for i in range(0, len(temp.network.params)):
          if (random.randint(0,100) < 60):
            # use strongest parent weights
            temp.network.params[i] = bestPreys[0].network.params[i]
          else:
            temp.network.params[i] = bestPreys[1].network.params[i]
      temp.update_speed(parent1_speed, parent2_speed)
      self.m_prey.append(temp)
      
  # Remove a predator from the world, making sure to update all other animats that the predator is no longer there.
  def remove_predator(self, predator):
    try:
      self.m_predators.remove(predator)
      unified = self.m_predators + self.m_prey
      for elem in unified:
        if elem.m_sees_pred == predator:
          elem.m_sees_pred = None
          elem.m_sees_pred_dist = 10000
        elif elem.m_touch_pred == predator:
          elem.m_touch_pred = None
        elem.m_chasing_pred = False
        elem.stop_being_chased(predator)
      return True
    except ValueError:
      return False
  
  # Remove a prey from the world, making sure to update all other animats about the new information.
  def remove_prey(self, prey):
    try:
      self.m_prey.remove(prey)
      unified = self.m_predators + self.m_prey
      for elem in unified:
        if elem.m_sees_prey == prey:
          elem.m_sees_prey = None
          elem.m_sees_prey_dist = 10000
        elif elem.m_touch_prey == prey:
          elem.m_touch_prey = None
        elem.m_chasing_prey = False
      return True
    except ValueError:
      return False
 
  def remove_fruit(self, fruit):
    try:
      self.m_fruit.remove(fruit)
      self.m_fruit_list.remove(fruit.m_rect)
      
      unified = self.m_predators + self.m_prey
      for elem in unified:
        if elem.seesFruit == fruit:
          elem.seesFruit = None
        elif elem.touchesFruit == fruit:
          elem.touchesFruit = None
      return True
    except ValueError:
      return False

  # Generate random coordinates that do not overlap with the two barriers.
  def get_safe_coordinates(self):
    rand1 = random.randint(1, self.m_width-10)
    rand2 = 0
    if rand1 > 260 and rand1 < 540:
      blah = random.randint(0, 2)
      if blah & 1:
        rand2 = random.randint(1, 180)
      else:
        rand2 = random.randint(420, 580)
    else:
      rand2 = random.randint(1, self.m_height -10)
    return [rand1, rand2]
    
  # Control the rate at which fruit is randomly generated. Initially, fruit is geneted at a high right but using a logit curve, reduce the amount of generated fruit until it is 0.
  def refresh_fruit(self):
    maxL = 500.0
    bounds = 2 * math.log((1/maxL) / (1 - (1/maxL)))
    while (len(self.m_fruit) < self.m_max_fruit):
      if (self.m_total_num_of_fruits_eaten >= maxL):
        self.m_total_num_of_fruits_eaten = maxL - 1
      x = self.m_total_num_of_fruits_eaten / maxL
      logit = math.log(x / (1 - x))
      rand = (random.random() - 0.5) * bounds
      if (rand < logit):
        break
      coords = self.get_safe_coordinates()
      new_fruit = Fruit.Fruit(coords[0], coords[1], 15, 15)
      self.m_fruit.append(new_fruit)
      self.m_fruit_list.append(new_fruit.m_rect)
